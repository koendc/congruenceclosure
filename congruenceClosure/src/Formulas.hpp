/*
 * Formulas.hpp
 *
 *  Created on: 25-nov.-2013
 *      Author: Koen
 */

#ifndef FORMULAS_HPP_
#define FORMULAS_HPP_

#include "Variable.hpp"
#include <string>
class CongruenceClosure;


class Formula {
public:
	virtual ~Formula();
	virtual Variable getLeftSide() = 0;
	virtual void accept(CongruenceClosure *alg) = 0;
	virtual std::string toString() = 0;
};

/**
 * Formula of the form a=b
 */
class ConstEq: public Formula{
public:
	ConstEq(Variable& var1, Variable& var2) : var1(var1), var2(var2) {}
	virtual Variable getLeftSide();
	Variable getRightSide();
	virtual void accept(CongruenceClosure *alg);
	virtual std::string toString();
private:
	Variable& var1;
	Variable& var2;
};

/**
 * Formula of the form a=f(a1,a2) so that if a1=b1 and a2=b2 => f(a1,a2)=f(b1,b2)
 */
class FuncEq: public Formula {
public:
	FuncEq(Variable& feval, Variable& arg1, Variable& arg2):
		feval(feval), firstArg(arg1), secondArg(arg2) {}
	virtual Variable getLeftSide();
	Variable getFirstArgument();
	Variable getSecondArgument();
	virtual void accept(CongruenceClosure *alg);
	virtual std::string toString();
private:
	Variable& feval;
	Variable& firstArg;
	Variable& secondArg;
};

/**
 * Formula of the form a=\=b
 */
class ConstInEq : public ConstEq{
public:
	ConstInEq(Variable& var1, Variable& var2) : ConstEq(var1,var2){}
	virtual void accept(CongruenceClosure *alg);
	virtual std::string toString();
};

/*
 * Formula of the form a = c(a1,a2) so that if a1=b1 and a2=b2 <=> c(a1,a2)=c(b1,b2)
 */
class ConstrEq : public FuncEq{
public:
	ConstrEq(Variable& ceval, Variable& arg1, Variable& arg2): FuncEq(ceval,arg1,arg2) {}
	virtual void accept(CongruenceClosure *alg);
	virtual std::string toString();
};

#endif /* FORMULAS_HPP_ */
