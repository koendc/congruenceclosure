/*
 * Variable.cpp
 *
 *  Created on: 28-nov.-2013
 *      Author: Koen
 */

#include "Variable.hpp"
#include "CongruenceGraph.hpp"
#include <sstream>

Variable::Variable(int varId) : varId(varId), node(Node(this, varId)) {}

Node Variable::getNode() {return node;}

CongruenceClass* Variable::getCongruenceClass() {
	return getNode().getCongruenceClass();
}

std::string Variable::toString() {
	std::stringstream out;
	out << getVarId();
	string result = "c" + out.str();
	return result;
}
