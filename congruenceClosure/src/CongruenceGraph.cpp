/*
 * CongruenceGraph.cpp
 *
 *  Created on: 26-nov.-2013
 *      Author: Koen
 */

#include "CongruenceGraph.hpp"
#include "CongruenceClosure.hpp"
#include <utility>
#include <iterator>
#include "Formulas.hpp"
#include "Variable.hpp"
#include <string>
#include <sstream>
#include <iostream>

CongruenceClass::CongruenceClass (Node* node, int varId) {
	nodes.insert(nodes.begin(), node);
	classId = varId;
	constructorFormula = NULL;
	constrFormTimestamp = 1;
}

bool CongruenceClass::hasInequalityEdgeTo(CongruenceClass *Kb){
	return ineqEdges.find(Kb)!=ineqEdges.end();
}

void CongruenceClass::addInequalityEdgeTo(CongruenceClass *Kb, int decisionlevel) {
	ineqEdges.insert(Kb);
	ineqEdgeTimestamps.insert({Kb,decisionlevel});
	if (Kb->hasInequalityEdgeTo(this))
		Kb->addInequalityEdgeTo(this, decisionlevel);
}

bool CongruenceClass::hasFormula(CongruenceClass* Kb){
	return formulas.find(Kb) != formulas.end();
}

void CongruenceClass::setFormula(CongruenceClass* Kb, FuncEq *form, int decisionlevel) {
	formulaTimestamps.insert({Kb,decisionlevel});
	formulas.insert({Kb, form});
	/*
	if (!Kb.hasFormula(*this))
		Kb.setFormula(*this, form);
	*/
}

FuncEq* CongruenceClass::getFormula(CongruenceClass* Kb) {
	return formulas[Kb];
}

bool CongruenceClass::hasConstructorFormula() {
	return constructorFormula != NULL;
}

void CongruenceClass::setConstructorFormula(ConstrEq* form, int decisionlevel){
	if (hasConstructorFormula())
		throw string("Class already has a constructor formula");
	constructorFormula = form;
	constrFormTimestamp = decisionlevel;
}

void CongruenceClass::addNode(Node* a, int decisionLevel) {
	nodes.insert(nodes.end(),a);
	a->setCongruenceClass(this, decisionLevel);

}

string CongruenceClass::getDescription(){
	string result = "K(";
	for (Node* node : nodes) {
		result += node->getDescription() + ",";
	}
	result = result.substr(0, result.length()-1);
	return result + ")";
}

Node::Node(Variable* var, int varId) : originalClass(CongruenceClass(this, varId)), var(var) {
	cclass = & originalClass;
}

void Node::setCongruenceClass(CongruenceClass* Ka, int decisionLevel) {
	while(prevClasses.size() < decisionLevel-1)
		prevClasses.push_back(cclass);
	cclass = Ka;
}

CongruenceClass* Node::getCongruenceClass(){
	return cclass;
}

string Node::getDescription() {
	std::stringstream out;
	out << var->getVarId();
	string result = out.str();
	return result;
}

bool CongruenceClass::isEqual(CongruenceClass* Ka) {
	return this->getClassId()==Ka->getClassId();
}

void CongruenceClass::revertTo(int decisionlevel) {
	// NODES
	list<Node*> newNodes;
	for (Node* node : nodes) {
		node->revertTo(decisionlevel);
		if (node->getCongruenceClass() == this)
			newNodes.push_back(node);
	}
	nodes = newNodes;

	//FORMULAS
	for (auto it = formulaTimestamps.begin(); it != formulaTimestamps.end(); it++){
		if (it->second > decisionlevel) {
			formulas.erase(it->first); // not necessary to remove time-stamps
		}
	}

	//INEQ-EDGES
	for (auto it = ineqEdgeTimestamps.begin(); it != ineqEdgeTimestamps.end(); it++){
		if (it->second > decisionlevel) {
			ineqEdges.erase(it->first);
		}
	}
}

void Node::revertTo(int decisionlevel){
	if (prevClasses.size() < decisionlevel) // node hasn't changed its class since this decision-level
		return;
	cclass = prevClasses.at(decisionlevel-1);
	auto start = prevClasses.begin();
	start += decisionlevel;
	prevClasses.erase(start, prevClasses.end());
}


