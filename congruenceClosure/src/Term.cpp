/*
 * Term.cpp
 *
 *  Created on: 28-nov.-2013
 *      Author: Koen
 */

#include "Term.hpp"

CongruenceClass* FuncTerm::Normalize() {
	CongruenceClass* Kt1 = firstArg->Normalize();
	CongruenceClass* Kt2 = secondArg->Normalize();
	if (Kt1->hasFormula(Kt2))
		return Kt1->getFormula(Kt2)->getLeftSide().getCongruenceClass();
	else
		return new FuncClass(Kt1,Kt2);
}

bool FuncTerm::isEqual(Term *t) {
	return t->isEqualFuncTerm(this);
}

bool FuncTerm::isEqualFuncTerm(FuncTerm *ft){
	return firstArg->isEqual(ft->getFirstArg()) && secondArg->isEqual(ft->getSecondArg());
}

list<Formula*> FuncTerm::convertToFormulas() {
	list<Formula*> formulas = firstArg->convertToFormulas();
	list<Formula*> form2 = secondArg->convertToFormulas();
	//TODO add a=f(a1,a2)
	formulas.insert(formulas.end(),form2.begin(),form2.end());
	return formulas;
}

CongruenceClass* ConstTerm::Normalize() {
	return var->getCongruenceClass();
}

bool ConstTerm::isEqual(Term *t) {
	return t->isEqualConstTerm(this);
}

bool ConstTerm::isEqualConstTerm(ConstTerm *ct) {
	return var->getVarId() == ct->getVariable()->getVarId();
}

list<Formula*> ConstTerm::convertToFormulas() {
	return list<Formula*>();//TODO create new var and return ConstEq
}

FuncClass::FuncClass(CongruenceClass* Ka, CongruenceClass* Kb) {
	firstArg = Ka;
	secondArg = Kb;
}

bool FuncClass::isEqual(CongruenceClass* Ka) {
	return Ka->isEqualFuncClass(this);
}

bool FuncClass::isEqualFuncClass(FuncClass *ft) {
	return firstArg->isEqual(ft->getFirstArg())
			&& secondArg->isEqual(ft->getSecondArg());
}
