/*
 * CongruenceGraph.cpp
 *
 *  Created on: 12-nov.-2013
 *      Author: Koen
 */

#ifndef CONGRUENCEGRAPH_HPP_
#define CONGRUENCEGRAPH_HPP_

using namespace std;

class Node;
class FuncClass;
class FuncEq;
class Variable;
class ConstrEq;

#include <list>
#include <set>
#include <unordered_map>
#include <map>
#include <algorithm>


class CongruenceClass {
public:
	CongruenceClass (Node* node, int varId);

	list<Node*> getNodes() { return nodes; }
	void addNode(Node* a, int decisionLevel);
	int getSize() {return nodes.size();}

	bool hasInequalityEdgeTo(CongruenceClass *Kb);
	void addInequalityEdgeTo(CongruenceClass *Kb, int decisionlevel);
	set<CongruenceClass*> getInequalityEdges() {return ineqEdges;}

	bool hasFormula(CongruenceClass* Kb);
	FuncEq * getFormula(CongruenceClass* Kb);
	void setFormula(CongruenceClass* Kb, FuncEq *form, int decisionlevel);
	bool hasConstructorFormula();
	ConstrEq * getConstructorFormula() {return constructorFormula;}
	void setConstructorFormula(ConstrEq* form, int decisionlevel);
	virtual bool isEqual(CongruenceClass* Ka);
	virtual bool isEqualFuncClass(FuncClass *ft) {return false;}
	int getClassId() {return classId;}
	string getDescription();
	void revertTo(int decisionlevel);
private:
	list<Node*> nodes;
	set<CongruenceClass*> ineqEdges;
	// timestamp for the ineq-edges, corresponding to the decisionlevel at which the ineq-formula was added
	map<CongruenceClass*, int> ineqEdgeTimestamps;
	unordered_map<CongruenceClass*, FuncEq*> formulas;
	// timestamp for the formula-edges, corresponding to the decisionlevel at which the formula was added
	map<CongruenceClass*, int> formulaTimestamps;
	ConstrEq* constructorFormula; // of the form a=c(a1,a2) so that a is in this class
	int constrFormTimestamp;
	int classId;
protected:
	CongruenceClass (){classId = 0; constructorFormula = NULL; constrFormTimestamp = 1;}
};


class Node {
public:
	Node(Variable* var, int varId);
	CongruenceClass* getCongruenceClass();
	Variable* getVar(){return var;}
	void setCongruenceClass(CongruenceClass* Ka, int decisionLevel);
	string getDescription();
	void revertTo(int decisionlevel);
private:
	CongruenceClass* cclass;
	CongruenceClass originalClass;
	Variable* var;
	vector<CongruenceClass*> prevClasses;
};

#endif /* CONGRUENCEGRAPH_HPP_*/
