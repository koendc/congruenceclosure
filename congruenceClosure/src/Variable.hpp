/*
 * Variable.hpp
 *
 *  Created on: 28-nov.-2013
 *      Author: Koen
 */

#ifndef VARIABLE_HPP_
#define VARIABLE_HPP_

#include <string>
#include "CongruenceGraph.hpp"

class CongruenceClass;

class Variable {
public:
	Variable(int varId);
	Variable() : varId(-1), node(Node(this, -1)){};
	int getVarId() const{ return varId;}
	Node getNode();
	CongruenceClass* getCongruenceClass();
	std::string toString();
private:
	const int varId;
	Node node;
};

#endif /* VARIABLE_HPP_ */
