/*
 * Equivalence.cpp
 *
 *  Created on: 12-nov.-2013
 *      Author: Koen
 */

using namespace std;

#include "Formulas.hpp"
#include "CongruenceClosure.hpp"
#include "Variable.hpp"
#include <iostream>

Formula::~Formula(){

}

Variable ConstEq::getLeftSide(){
	return var1;
}
Variable ConstEq::getRightSide(){
	return var2;
}

Variable FuncEq::getFirstArgument(){
	return firstArg;
}
Variable FuncEq::getSecondArgument(){
	return secondArg;
}
Variable FuncEq::getLeftSide(){
	return feval;
}

void ConstEq::accept(CongruenceClosure *alg) {
	EqLabel label = EqLabel(this);
	alg->joinClasses(getLeftSide().getCongruenceClass(),
			getRightSide().getCongruenceClass(),
			label);
}
void FuncEq::accept(CongruenceClosure *alg) {
	CongruenceClass* Ka1 = firstArg.getCongruenceClass();
	CongruenceClass* Ka2 = secondArg.getCongruenceClass();
	if (Ka1->hasFormula(Ka2)) {
		CongruenceClass* Ka = getLeftSide().getCongruenceClass();
		CongruenceClass* Kb = Ka1->getFormula(Ka2)->getLeftSide().getCongruenceClass();
		FuncLabel label = FuncLabel(this, Ka1->getFormula(Ka2));
		alg->joinClasses(Ka,Kb,label);
	} else {
		Ka1->setFormula(Ka2,this, alg->getCurrentDecisionLevel());
	}
}
void ConstInEq::accept(CongruenceClosure *alg) {
	CongruenceClass* Ka = getLeftSide().getCongruenceClass();
	CongruenceClass* Kb = getRightSide().getCongruenceClass();
	if (Ka->isEqual(Kb)) {
		alg->reportMergeConflict(this);
	} else {
		Ka->addInequalityEdgeTo(Kb, alg->getCurrentDecisionLevel());
		alg->addIneqEdgeToProofForest(this);
	}
}

void ConstrEq::accept(CongruenceClosure* alg) {
	FuncEq::accept(alg);
	CongruenceClass* Ka = getLeftSide().getCongruenceClass();
	if (Ka->hasConstructorFormula()) { // There is another constructor formula that is equal to this one
		CongruenceClass* Ka1 = getFirstArgument().getCongruenceClass();
		CongruenceClass* Ka2 = getSecondArgument().getCongruenceClass();
		ConstrEq* form2 = Ka->getConstructorFormula();
		CongruenceClass* Kb1 = form2->getFirstArgument().getCongruenceClass();
		CongruenceClass* Kb2 = form2->getSecondArgument().getCongruenceClass();

		ConstructorLabel label1 = ConstructorLabel(this, form2, true);
		ConstructorLabel label2 = ConstructorLabel(this, form2, false);
		alg->joinClasses(Ka1,Kb1,label1);
		alg->joinClasses(Ka2,Kb2,label2);
	} else {
		Ka->setConstructorFormula(this, alg->getCurrentDecisionLevel());
	}
}

string ConstEq::toString() {
	string result = getLeftSide().toString()
			+ " = " +
			getRightSide().toString();
	return result;
}

string FuncEq::toString() {
	string result = getLeftSide().toString()
				+ " = f(" +
				getFirstArgument().toString()
				+ ", " + getSecondArgument().toString()
				+ ")";
	return result;
}

string ConstInEq::toString() {
	string result = getLeftSide().toString()
				+ " =/= " +
				getRightSide().toString();
	return result;
}

string ConstrEq::toString() {
	string result = getLeftSide().toString()
					+ " = c(" +
					getFirstArgument().toString()
					+ ", " + getSecondArgument().toString()
					+ ")";
		return result;
}
