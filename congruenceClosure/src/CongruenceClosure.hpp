/*
 * CongruenceClosure.hpp
 *
 *  Created on: 26-nov.-2013
 *      Author: Koen
 */

#ifndef CONGRUENCECLOSURE_HPP_
#define CONGRUENCECLOSURE_HPP_

#include "CongruenceGraph.hpp"
#include "Variable.hpp"
#include "Formulas.hpp"
#include "Term.hpp"
#include "proof/ProofForest.hpp"
#include "proof/ProofLabel.hpp"
#include <list>
#include <unordered_set>
#include <memory>
#include <mutex>

class CongruenceClosure {
public:
	CongruenceClosure();
	~CongruenceClosure();
	CongruenceClosure(const CongruenceClosure&) = delete;
	void merge(Formula& t);
	bool isCongruent(Term& s, Term& t);
	bool isCongruent(Variable& a, Variable& b);
	void reportJoinConflict(ProofLabel& source);
	void reportMergeConflict(ConstInEq* source);
	unordered_set<Formula*> explain(Variable a, Variable b);
	unordered_set<Formula*> explain(ProofLabel& label);
	unordered_set<Formula*> explainConflict(Variable a, Variable b);
	void joinClasses(CongruenceClass* Ka, CongruenceClass* Kb, ProofLabel& label);
	void addIneqEdgeToProofForest(ConstInEq* ineq);
	string getCongruenceDescr();
	Variable& makeNewVariable();
	void setDecisionLevel(int newDecisionLevel);
	void backtrackTo(int decisionLevel);
	int getCurrentDecisionLevel() {return currentDecisionLevel;}
private:
	void addNewVariable(Variable& a);
	list<FormLabel*> propagateJoins(CongruenceClass* Ka, CongruenceClass* Kb);
	list<CongruenceClass*> congClasses;
	list<std::shared_ptr<Variable>> variables;
	ProofForest* proofForest;
	int nextVarId;
	int currentDecisionLevel;
	mutex newVarMutex;
};



#endif /* CONGRUENCECLOSURE_HPP_ */
