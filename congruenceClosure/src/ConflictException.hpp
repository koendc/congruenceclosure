/*
 * ConflictException.hpp
 *
 *  Created on: 3-dec.-2013
 *      Author: Koen
 */

#ifndef CONFLICTEXCEPTION_HPP_
#define CONFLICTEXCEPTION_HPP_

using namespace std;

#include <exception>
#include "proof/ProofLabel.hpp"
#include "CongruenceClosure.hpp"
#include <unordered_set>

class ConflictException : public std::exception {
public:
	ConflictException(CongruenceClosure* alg) {this->alg = alg;}
	virtual ~ConflictException() throw() {}
	virtual unordered_set<Formula*> explain() = 0;
private:
	CongruenceClosure* alg;
protected:
	CongruenceClosure* getAlgorithm() {return alg;}
};

class JoinConflictException : public ConflictException {
public:
	JoinConflictException(CongruenceClosure* alg, ProofLabel& source) :
		ConflictException(alg),
		source(source.makeNewLabel()){}
	~ JoinConflictException() throw() {delete source;}
	virtual unordered_set<Formula*> explain();
private:
	ProofLabel* source;
};

class MergeConflictException : public ConflictException {
public:
	MergeConflictException(CongruenceClosure* alg, ConstInEq* source) : ConflictException(alg){
		this->source = source;}
	virtual unordered_set<Formula*> explain();
private:
	ConstInEq* source;
};


#endif /* CONFLICTEXCEPTION_HPP_ */
