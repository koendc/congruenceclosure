/*
 * Term.hpp
 *
 *  Created on: 28-nov.-2013
 *      Author: Koen
 */

#ifndef TERM_HPP_
#define TERM_HPP_

#include "CongruenceGraph.hpp"
#include "Formulas.hpp"

class FuncTerm;
class ConstTerm;

class Term {
public:
	virtual CongruenceClass* Normalize() =0;
	virtual ~Term(){}
	virtual bool isEqual(Term *t)=0;
	virtual bool isEqualFuncTerm(FuncTerm *ft) {return false;}
	virtual bool isEqualConstTerm(ConstTerm *ct) {return false;}
	virtual list<Formula*> convertToFormulas()=0;
protected:
	Term() {}
};

class FuncTerm : public Term {
public:
	FuncTerm(Term* arg0, Term* arg1){firstArg = arg0;secondArg=arg1;}
	~FuncTerm() {delete firstArg;delete secondArg;}
	Term* getFirstArg() {return firstArg;}
	Term* getSecondArg() {return secondArg;}
	virtual bool isEqual(Term *t);
	virtual bool isEqualFuncTerm(FuncTerm *ft);
//	virtual bool isEqualConstTerm(ConstTerm *ct);
	virtual CongruenceClass* Normalize();
	list<Formula*> convertToFormulas();
private:
	Term* firstArg;
	Term* secondArg;
};

class ConstTerm : public Term {
public:
	ConstTerm(Variable* var){this->var = var;}
	~ConstTerm(){}
	Variable* getVariable() {return var;}
	virtual bool isEqual(Term *t);
//	virtual bool isEqualFuncTerm(FuncTerm *ft);
	virtual bool isEqualConstTerm(ConstTerm *ct);
	virtual CongruenceClass* Normalize();
	list<Formula*> convertToFormulas();
private:
	Variable* var;
};

class FuncClass : public CongruenceClass {
public:
	FuncClass(CongruenceClass*,CongruenceClass*);
	CongruenceClass* getFirstArg() {return firstArg;}
	CongruenceClass* getSecondArg() {return secondArg;}
	virtual bool isEqual(CongruenceClass* Ka);
	virtual bool isEqualFuncClass(FuncClass *ft);
private:
	CongruenceClass* firstArg;
	CongruenceClass* secondArg;
};




#endif /* TERM_HPP_ */
