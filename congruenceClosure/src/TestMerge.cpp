/*
 * TestMerge.cpp
 *
 *  Created on: 30-nov.-2013
 *      Author: Koen
 */

#include "CongruenceClosure.hpp"
#include "ConflictException.hpp"
#include "Variable.hpp"
#include "Formulas.hpp"
#include <iostream>
#include <unordered_set>

class MergeTest {
private:
	bool includesFormula(unordered_set<Formula*> explanation, Formula& formula) {
		return std::count(explanation.begin(), explanation.end(), & formula) == 1;
	}
	void printFailed(string test){
		cout << "#  " + test + " failed\n";
	}
	void printSuccess(string test){
		cout << "   " + test + " succeeded\n";
	}
	void printExplanation(unordered_set<Formula*> explanation) {
		string result = "(";
		for (Formula* form : explanation)
			result += form->toString() + ", ";
		result = result.substr(0, result.length()-2);
		cout << result + ")\n";
	}
	CongruenceClosure alg;
	Variable& c1;
	Variable& c2;
	Variable& c3;
	Variable& c4;
	Variable& c5;

public:
	MergeTest() :
	c1(alg.makeNewVariable()),
	c2(alg.makeNewVariable()),
	c3(alg.makeNewVariable()),
	c4(alg.makeNewVariable()),
	c5(alg.makeNewVariable()) {}

	/**
	 * Test whether 'isCongruent' succeeds on a positive example
	 */
	void test1() {
		ConstEq form3 = ConstEq(c2, c4);

		alg.merge(form3);
		ConstTerm s = ConstTerm(&c2);
		ConstTerm t = ConstTerm(&c4);

		if (alg.isCongruent(s,t)){
			printSuccess("test1");
		} else {
			printFailed("test1");
		}
	}

	/*
	 * Test whether 'isCongruent' succeeds on a propagated example
	 */
	void test2() {
		FuncEq form1 = FuncEq(c1, c5, c2);
		FuncEq form2 = FuncEq(c3, c5, c4);
		ConstEq form3 = ConstEq(c2, c4);

		//		std::cout<<alg.getCongruenceDescr();
		alg.merge(form1);
		alg.merge(form2);
		alg.merge(form3);
		//		std::cout<<alg.getCongruenceDescr();

		if (alg.isCongruent(c1, c3)){
			printSuccess("test2");
		} else {
			printFailed("test2");
		}

	}
	/*
	 * Test whether 'isCongruent' succeeds on a propagated example
	 */
	void test2b() {
		Variable& c6 = alg.makeNewVariable();
		FuncEq form1 = FuncEq(c1, c5, c2);
		ConstEq form2 = ConstEq(c6,c2);
		FuncEq form3 = FuncEq(c3, c5, c4);
		ConstEq form4 = ConstEq(c2, c4);

		//		std::cout<<alg.getCongruenceDescr();
		alg.merge(form1);
		alg.merge(form2);
		alg.merge(form3);
		alg.merge(form4);
		//		std::cout<<alg.getCongruenceDescr();

		ConstTerm s = ConstTerm(&c1);
		ConstTerm t = ConstTerm(&c3);

		if (alg.isCongruent(s, t)){
			printSuccess("test2b");
		} else {
			printFailed("test2b");
		}
	}

	/*
	 * Test whether 'isCongruent' successfully decides on a negative example
	 */
	void test3() {
		ConstEq form3 = ConstEq(c2, c4);

		alg.merge(form3);

		ConstTerm s = ConstTerm(&c1);
		ConstTerm t = ConstTerm(&c3);

		if (!alg.isCongruent(s, t)){
			printSuccess("test3");
		} else {
			printFailed("test3");
		}
	}

	/**
	 * Test whether a conflict is thrown with the addition of an inequality
	 */
	void test4() {
		ConstEq form3 = ConstEq(c2,c4);

		alg.merge(form3);

		ConstInEq form4 = ConstInEq(c2,c4);

		try {
			alg.merge(form4);
			printFailed("test4");
		} catch (ConflictException& e) {
			printSuccess("test4");
		}
	}

	/**
	 * Test whether inequalities trigger conflicts only when needed
	 */
	void test5() {
		ConstInEq form4 = ConstInEq(c2,c4);

		try {
			alg.merge(form4);
			printSuccess("test5");
		} catch (ConflictException& e) {
			printFailed("test5");
		}
	}

	/**
	 * Test whether a conflict is thrown after a merge of two classes with an inequality edge
	 */
	void test6() {
		try {
			ConstEq form3 = ConstEq(c2, c4);
			ConstInEq form4 = ConstInEq(c2,c4);

			alg.merge(form4);

			try {
				alg.merge(form3);
				printFailed("test6");
			} catch (ConflictException& e) {
				printSuccess("test6");
			}
		} catch (ConflictException& e) {
			printFailed("test6");
		}
	}

	/**
	 * Test whether inequalities are merged when joining classes
	 */
	void test7() {
		try {
			ConstInEq form1 = ConstInEq(c1, c2);
			ConstInEq form2 = ConstInEq(c3, c4);
			ConstEq form3 = ConstEq(c2, c3);
			ConstEq form4 = ConstEq(c1, c3);
			ConstEq form5 = ConstEq(c2, c4);

			alg.merge(form1);
			alg.merge(form2);
			alg.merge(form3);

			try {
				alg.merge(form4);
				printFailed("test7: no conflict thrown");
			} catch (ConflictException& e) {
				try {
					alg.merge(form5);
					printFailed("test7: no conflict thrown");
				} catch (ConflictException& e) {
					printSuccess("test7");
				}
			}
		} catch (ConflictException& e) {
			printFailed("test7: unexpected conflict");
		}
	}
	/*
	 * Tests explanations are generated correctly for simple cases.
	 */
	void test8() {
		ConstEq form3 = ConstEq(c2, c4);

		try {
			alg.merge(form3);
			unordered_set<Formula*> explanation = alg.explain(c2,c4);

			if (explanation.size() == 1 &&
					includesFormula(explanation, form3)){
				printSuccess("test8");
			} else {
				printFailed("test8");
			}
		} catch (string& e) {
			printFailed("test8: " + e);
		}
	}
	/*
	 * Test whether explanations are generated correctly when including function-formulas.
	 */
	void test9() {
		FuncEq form1 = FuncEq(c1, c5, c2);
		FuncEq form2 = FuncEq(c3, c5, c4);
		ConstEq form3 = ConstEq(c2, c4);

		alg.merge(form1);
		alg.merge(form2);
		alg.merge(form3);

		try {
			unordered_set<Formula*> explanation = alg.explain(c1,c3);
			//			printExplanation(explanation);

			if (explanation.size() == 3 &&
					includesFormula(explanation, form1) &&
					includesFormula(explanation, form2) &&
					includesFormula(explanation, form3)){
				printSuccess("test9");
			} else {
				printFailed("test9");
			}
		} catch (string& e) {
			printFailed("test8: " + e);
		}
	}

	/*
	 * Test whether explanations only include the relevant formulas.
	 */
	void test10() {
		FuncEq form1 = FuncEq(c1, c5, c2);
		FuncEq form2 = FuncEq(c3, c5, c4);
		ConstEq form3 = ConstEq(c2, c4);

		alg.merge(form1);
		alg.merge(form2);
		alg.merge(form3);

		try {
			unordered_set<Formula*> explanation = alg.explain(c2,c4);
			//				printExplanation(explanation);

			if (explanation.size() == 1 &&
					includesFormula(explanation, form3)){
				printSuccess("test10");
			} else {
				printFailed("test10");
			}
		} catch (string& e) {
			printFailed("test8: " + e);
		}
	}
	/**
	 * Test whether the explanation of a conflict after merging a=/=b is correct
	 */
	void test11() {
		ConstEq form3 = ConstEq(c2, c4);
		ConstInEq form4 = ConstInEq(c2,c4);

		alg.merge(form3);

		try {
			alg.merge(form4);
		} catch (ConflictException& e) {
			unordered_set<Formula*> explanation = e.explain();
			if (explanation.size() == 2 &&
					includesFormula(explanation, form4)) {
				printSuccess("test11");
			} else
				printFailed("test11");
		}
	}
	/**
	 * Test whether the explanation of a conflict after an invalid join is correct
	 */
	void test12() {
		try {
			ConstEq form3 = ConstEq(c2, c4);
			ConstInEq form4 = ConstInEq(c2,c4);

			alg.merge(form4);

			try {
				alg.merge(form3);
			} catch (ConflictException& e) {
				unordered_set<Formula*> explanation = e.explain();
				if (explanation.size() == 2 &&
						includesFormula(explanation, form4)) {
					printSuccess("test12");
				} else
					printFailed("test12");
					printExplanation(explanation);
			}
		} catch (ConflictException& e) {
			printFailed("test12");
		}
	}
	/*
	 * A more complicated test for explanation of a conflict to test whether ineq-edges are passed
	 * correctly through the proof tree when the root changes.
	 */
	void test13() {
		try {
			Variable& c6 = alg.makeNewVariable();
			Variable& c7 = alg.makeNewVariable();

			FuncEq func1 = FuncEq(c1,c5,c6);
			FuncEq func2 = FuncEq(c2,c5,c7);
			ConstEq form2 = ConstEq(c3, c2);
			ConstEq form3 = ConstEq(c4, c6);
			ConstEq form4 = ConstEq(c7, c6);
			ConstInEq form5 = ConstInEq(c3,c4);
			ConstEq form6 = ConstEq(c1, c7);

			alg.merge(form5);
			alg.merge(func1);
			alg.merge(func2);
			alg.merge(form2);
			alg.merge(form3);
			alg.merge(form4);

			try {
				alg.merge(form6);
				printFailed("test13: no conflict thrown");
			} catch (ConflictException& e) {
				unordered_set<Formula*> explanation = e.explain();
				if (explanation.size() == 7) {
					printSuccess("test13");
					//					printExplanation(explanation);
				} else {
					printFailed("test13 : explanation of incorrect size");
					printExplanation(explanation);
				}
			} catch (string& s) {
				printFailed("test13: " + s);
			}
		} catch (string* e) {
			printFailed("test13: " + *e);
		}

	}
	/*
	 * Test example 14 of the paper of Oliveras.
	 */
	void test14() {
		Variable& c6 = alg.makeNewVariable();
		Variable& c7 = alg.makeNewVariable(); Variable& c8 = alg.makeNewVariable();
		Variable& c9 = alg.makeNewVariable(); Variable& c10 = alg.makeNewVariable();
		Variable& c11 = alg.makeNewVariable(); Variable& c12 = alg.makeNewVariable();
		Variable& c13 = alg.makeNewVariable(); Variable& c14 = alg.makeNewVariable();

		ConstEq form1 =  ConstEq(c1,c8); ConstEq form2 =  ConstEq(c7,c2);
		ConstEq form3 =  ConstEq(c3,c13); ConstEq form4 =  ConstEq(c7,c1);
		ConstEq form5 =  ConstEq(c6,c7); ConstEq form6 =  ConstEq(c9,c5);
		ConstEq form7 =  ConstEq(c9,c3); ConstEq form8 =  ConstEq(c14,c11);
		ConstEq form9 =  ConstEq(c10,c4); ConstEq form10 =  ConstEq(c12,c9);
		ConstEq form11 =  ConstEq(c4,c11); ConstEq form12 =  ConstEq(c10,c7);

		try {
			alg.merge(form1); alg.merge(form2);
			alg.merge(form3); alg.merge(form4);
			alg.merge(form5); alg.merge(form6);
			alg.merge(form7); alg.merge(form8);
			alg.merge(form9); alg.merge(form10);
			alg.merge(form11); alg.merge(form12);

			unordered_set<Formula*> explanation = alg.explain(c5,c13);
			//				printExplanation(explanation);

			if (explanation.size() >1 &&
					includesFormula(explanation, form6)){
				printSuccess("test14");
				printExplanation(explanation);
			} else {
				printFailed("test14");
				printExplanation(explanation);
			}
		} catch (ConflictException& e) {
			printFailed("test14: unexpected conflict");
			printExplanation(e.explain());
		} catch (string& e) {
			printFailed("test14: " + e);
		}
	}
	/*
	 * test example 16 of the paper of Oliveras
	 */
	void test14b(){
		Variable& a = c1; Variable& b = c2;
		Variable& c = c3; Variable& d = c4;
		Variable& e = c5; Variable& f = alg.makeNewVariable();
		Variable& g = alg.makeNewVariable();
		Variable& h = alg.makeNewVariable();

		FuncEq form1 = FuncEq(d,g,h); // d = f(g,h)
		ConstEq form2 = ConstEq(c,d);	// c = d
		FuncEq form3 = FuncEq(a,g,d); // a = f(g,d)
		ConstEq form4 = ConstEq(e,c);	// e = c
		ConstEq form5 = ConstEq(e,b);	// e = b
		ConstEq form6 = ConstEq(b,h);	// b = h

		try {
			alg.merge(form1); alg.merge(form2);
			alg.merge(form3); alg.merge(form4);
			alg.merge(form5); alg.merge(form6);

			unordered_set<Formula*> explanation = alg.explain(a,h);
			if (explanation.size() >1 &&
					includesFormula(explanation, form6)){
				printSuccess("test14b");
				printExplanation(explanation);
			} else {
				printFailed("test14b");
				printExplanation(explanation);
			}
		} catch(ConflictException& e) {
			printFailed("test14b");
			printExplanation(e.explain());
		} catch(string& e) {
			printFailed("test14b: " + e);
		} catch(...) {
			printFailed("test14b: unknown error");
		}
	}
	/*
	 * Test backtracking on congruence classes
	 */
	void test15(){
		ConstEq form1 = ConstEq(c1,c2);
		ConstEq form2 = ConstEq(c2,c3);
		ConstEq form3 = ConstEq(c3,c4);

		alg.merge(form1);
		alg.setDecisionLevel(2);
		alg.merge(form2);
		alg.setDecisionLevel(3);
		alg.merge(form3);
		try {
			alg.backtrackTo(1);

			ConstTerm s = ConstTerm(&c1);
			ConstTerm t = ConstTerm(&c2);
			ConstTerm u = ConstTerm(&c3);
			ConstTerm v = ConstTerm(&c4);
			if (!alg.isCongruent(s,t))
				printFailed("test15: c1 and c2 not congruent");
			else if (alg.isCongruent(t,u))
				printFailed("test15: c2 and c3 congruent");
			else if (alg.isCongruent(u,v))
				printFailed("test15: c3 and c4 congruent");
			else
				printSuccess("test15");
		} catch (...) {
			printFailed("test15: exception");
		}
	}
	/**
	 * Test backtracking with function formulas
	 */
	void test15b(){
		ConstEq form1 = ConstEq(c1,c2);
		FuncEq form2 = FuncEq(c3,c5,c1);
		FuncEq form3 = FuncEq(c4,c5,c2);

		alg.merge(form1);
		alg.setDecisionLevel(2);
		alg.merge(form2);
		alg.setDecisionLevel(3);
		alg.merge(form3);
		try {
			alg.backtrackTo(2);
			alg.merge(form3);

			ConstTerm s = ConstTerm(&c1);
			ConstTerm t = ConstTerm(&c2);
			ConstTerm u = ConstTerm(&c3);
			ConstTerm v = ConstTerm(&c4);
			if (!alg.isCongruent(s,t))
				printFailed("test15b: c1 and c2 not congruent");
			else if (!alg.isCongruent(u,v))
				printFailed("test15b: c3 and c4 not congruent");
			else
				printSuccess("test15b");
		} catch (...) {
			printFailed("test15b: exception");
		}
	}
	/*
	 * Test backtracking on inequalities
	 */
	void test15c(){
		ConstEq form1 = ConstEq(c1,c2);
		ConstEq form2 = ConstEq(c2,c3);
		ConstEq form3 = ConstEq(c3,c4);
		ConstInEq form4 = ConstInEq(c1,c4);

		alg.merge(form1);
		alg.setDecisionLevel(2);
		alg.merge(form2);
		alg.setDecisionLevel(3);
		alg.merge(form4);
		try {
			alg.backtrackTo(2);
			alg.merge(form3);
			printSuccess("test15c");
		} catch (ConflictException& e) {
			printFailed("test15c: conflict");
			printExplanation(e.explain());
		} catch (...) {
			printFailed("test15c: exception");
		}
	}
	/**
	 * Test backtracking with explanations
	 */
	void test15d(){
		ConstEq form1 = ConstEq(c1,c2);
		FuncEq form2 = FuncEq(c3,c5,c1);
		FuncEq form3 = FuncEq(c4,c5,c2);
		ConstEq form4 = ConstEq(c2,c3);

		alg.merge(form1);
		alg.merge(form2);
		alg.merge(form3);
		alg.setDecisionLevel(2);
		alg.merge(form4);
		try {
			alg.backtrackTo(1);
			alg.explain(c1,c2);
			alg.explain(c3,c4);
			printSuccess("test15d");
		} catch (string& e) {
			printFailed("test15d: " + e);
		}
	}

	void test16(){
		Variable& c6 = alg.makeNewVariable(); Variable& c7 = alg.makeNewVariable();
		Variable& c8 = alg.makeNewVariable(); Variable& c9 = alg.makeNewVariable();


		ConstrEq form1 = ConstrEq(c2,c6,c7);
		ConstrEq form2 = ConstrEq(c4,c8,c9);
		ConstEq form3 = ConstEq(c1,c3);
		ConstrEq form4 = ConstrEq(c1,c5,c2);
		ConstrEq form5 = ConstrEq(c3,c5,c4);

		try {
			alg.merge(form1); alg.merge(form2);
			alg.merge(form3); alg.merge(form4);
			alg.merge(form5);

			if (!alg.isCongruent(c2,c4))
				printFailed("test16: c2 and c4 not congruent");
			else if (!alg.isCongruent(c6,c8))
				printFailed("test16: c6 and c8 not congruent");
			else if (!alg.isCongruent(c7,c9))
				printFailed("test16: c7 and c9 not congruent");
			else {
				unordered_set<Formula*> explanation = alg.explain(c6,c8);
				printSuccess("test16");
				printExplanation(explanation);
			}
		} catch(...) {
			printFailed("test16");
		}
	}

};

int main(int argc, char **argv) {
	std::cout<<"Testing merge procedure:\n";
	MergeTest().test1();	MergeTest().test2(); MergeTest().test2b();
	MergeTest().test3();	MergeTest().test4();
	MergeTest().test5();	MergeTest().test6();
	MergeTest().test7();	MergeTest().test8();
	MergeTest().test9();	MergeTest().test10();
	MergeTest().test11();	MergeTest().test12();
	MergeTest().test13();	MergeTest().test14(); MergeTest().test14b();
	MergeTest().test15();	MergeTest().test15b(); MergeTest().test15c(); MergeTest().test15d();
	MergeTest().test16();
}



