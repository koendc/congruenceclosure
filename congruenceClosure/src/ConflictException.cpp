/*
 * ConflictException.cpp
 *
 *  Created on: 12-feb.-2014
 *      Author: Koen
 */
# include "ConflictException.hpp"


unordered_set<Formula*> JoinConflictException::explain() {
	Variable a = source->getFirstVar();
	Variable b = source->getSecondVar();
	unordered_set<Formula*> explanation = getAlgorithm()->explainConflict(a,b); // explain the conflict between a and b
	unordered_set<Formula*> expl2 = getAlgorithm()->explain(*source); // explain the join
	explanation.insert(expl2.begin(), expl2.end());

	return explanation;
}

unordered_set<Formula*> MergeConflictException::explain() {
	Variable a = source->getLeftSide();
	Variable b = source->getRightSide();
	unordered_set<Formula*> explanation = getAlgorithm()->explain(a,b); // explain why a and b are congruent
	explanation.insert(source); // add a =/= b to the explanation

	return explanation;
}
