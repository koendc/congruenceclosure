/*
 * ProofForest.hpp
 *
 *  Created on: 23-jan.-2014
 *      Author: Koen
 */

#ifndef PROOFFOREST_HPP_
#define PROOFFOREST_HPP_

using namespace std;

#include "../Variable.hpp"
#include "../Formulas.hpp"
#include <map>
#include <list>
#include <memory>
#include <unordered_set>
#include "ProofLabel.hpp"

class ProofNode {
public:
	ProofNode(Variable& a);
	~ProofNode();
	Variable getVariable() {return variable;}
	ProofNode* getParent() {return parent;}
	ProofLabel* getLabel() {return edgeLabel;}
	int getEdgeTimestamp() {return edgeTimeStamp;}
	ConstInEq* getConflictFormula(ProofNode* rb) {return ineqEdges[rb];}
	void setParent(ProofNode* b) {parent = b;}
	void setLabel(ProofLabel& label, int timestamp);
	void addIneqEdge(ProofNode* rb, ConstInEq* form);
	void removeIneqEdge(ProofNode* b);
	void transferIneqEdgesToRoot();
	bool isRoot();
	bool hasIneqEdge(ProofNode* rb) {return ineqEdges.count(rb) != 0;}
	void revertTo(int decisionlevel);
private:
	void clearIneqEdges();
	Variable variable;
	ProofNode* parent;
	ProofLabel* edgeLabel;
	int edgeTimeStamp;
	map<ProofNode*, ConstInEq*> ineqEdges;
	map<ProofNode*, int> ineqEdgeTimestamps;
};

class ProofForest {
public:
	ProofForest();
	~ProofForest();
	void addNode(Variable& a);
	void addEdge(ProofLabel& label, int timestamp);
	void addIneqEdge(ConstInEq* ineq);
	Variable nearestCommonAncestor(Variable a, Variable b);
	unordered_set<Formula*> explainPath(Variable a, Variable c);
	unordered_set<Formula*> explain(Variable a, Variable b);
	unordered_set<Formula*> explainConflict(Variable a, Variable b);
	void revertTo(int decisionlevel);
private:
	void setEdgeWLabel(ProofNode* a, ProofNode* b, ProofLabel& label, int timestamp);
	void invertAllEdgesToRoot(ProofNode* a);
	//ProofNode* getRoot(ProofNode* a);
	ProofNode* getNode(const Variable a);
	map<int, std::shared_ptr<ProofNode>> proofNodes;
};



#endif /* PROOFFOREST_HPP_ */
