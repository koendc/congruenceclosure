/*
 * ProofLabel.hpp
 *
 *  Created on: 7-feb.-2014
 *      Author: Koen
 */

#ifndef PROOFLABEL_HPP_
#define PROOFLABEL_HPP_

using namespace std;

#include "../Variable.hpp"
#include "../Formulas.hpp"
#include <map>
#include <list>
#include <unordered_set>

class ProofForest;

class ProofLabel {
public:
	ProofLabel() {};
	virtual unordered_set<Formula*> explain(ProofForest* proofForest)=0;
	virtual ~ProofLabel() {};
	virtual Variable getFirstVar()=0;
	virtual Variable getSecondVar()=0;
	virtual ProofLabel* makeNewLabel()=0;
	virtual bool isSelfLabel()=0;
private:
};

class EqLabel: public ProofLabel {
public:
	EqLabel(ConstEq* eq) {form = eq;}
	~EqLabel() {/*delete form;*/ }
	unordered_set<Formula*> explain(ProofForest* proofForest);
	Variable getFirstVar();
	Variable getSecondVar();
	ProofLabel* makeNewLabel();
	bool isSelfLabel() {return false;}
private:
	ConstEq* form;
};

class FormLabel: public ProofLabel {
public:
	virtual Variable getFirstVar() = 0;
	virtual Variable getSecondVar() = 0;
	bool isSelfLabel() {return false;}
};

class FuncLabel: public FormLabel {
public:
	FuncLabel(FuncEq* f1, FuncEq* f2) {form1=f1; form2=f2;}
	~FuncLabel() {/*delete form1; delete form2;*/}
	unordered_set<Formula*> explain(ProofForest* proofForest);
	Variable getFirstVar();
	Variable getSecondVar();
	ProofLabel* makeNewLabel();
private:
	FuncEq* form1;
	FuncEq* form2;
};

class SelfLabel: public ProofLabel {
public:
	unordered_set<Formula*> explain(ProofForest* proofForest);
	Variable getFirstVar() {throw "Not applicable to self-label";}
	Variable getSecondVar() {throw "Not applicable to self-label";}
	static SelfLabel& getInstance() {static SelfLabel label; return label;}
	ProofLabel* makeNewLabel();
	bool isSelfLabel() {return true;}
};

class ConstructorLabel: public FormLabel {
public:
	ConstructorLabel(ConstrEq* f1, ConstrEq* f2, bool firstArgs) : form1(f1), form2(f2), firstArgs(firstArgs){}
	unordered_set<Formula*> explain(ProofForest* proofForest);
	Variable getFirstVar();
	Variable getSecondVar();
	ProofLabel* makeNewLabel();
private:
	ConstrEq* form1;
	ConstrEq* form2;
	bool firstArgs;
};

#endif /* PROOFLABEL_HPP_ */
