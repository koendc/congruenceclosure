/*
 * ProofLabel.cpp
 *
 *  Created on: 7-feb.-2014
 *      Author: Koen
 */

#include "ProofLabel.hpp"
#include "ProofForest.hpp"

unordered_set<Formula*> EqLabel::explain(ProofForest* ProofForest) {
	unordered_set<Formula*> explanation;
	explanation.insert(form);
	return explanation;
}

Variable EqLabel::getFirstVar() {
	return form->getLeftSide();
}
Variable EqLabel::getSecondVar() {
	return form->getRightSide();
}

unordered_set<Formula*> FuncLabel::explain(ProofForest* proofForest) {
	unordered_set<Formula*> explanation;
	explanation.insert(form1);
	explanation.insert(form2);
	unordered_set<Formula*> expl1 = proofForest->explain(form1->getFirstArgument(),form2->getFirstArgument());
	unordered_set<Formula*> expl2 = proofForest->explain(form1->getSecondArgument(),form2->getSecondArgument());
	explanation.insert(expl1.begin(), expl1.end());
	explanation.insert(expl2.begin(), expl2.end());
	return explanation;
}

Variable FuncLabel::getFirstVar() {
	return form1->getLeftSide();
}
Variable FuncLabel::getSecondVar() {
	return form2->getLeftSide();
}

unordered_set<Formula*> SelfLabel::explain(ProofForest* proofForest) {
	return unordered_set<Formula*>();
}

unordered_set<Formula*> ConstructorLabel::explain(ProofForest* proofForest) {
	unordered_set<Formula*> explanation;
	explanation.insert(form1);
	explanation.insert(form2);
	unordered_set<Formula*> expl1 = proofForest->explain(form1->getLeftSide(),form2->getLeftSide());
	explanation.insert(expl1.begin(), expl1.end());
	return explanation;
}

Variable ConstructorLabel::getFirstVar(){
	if (firstArgs)
		return form1->getFirstArgument();
	else
		return form1->getSecondArgument();
}

Variable ConstructorLabel::getSecondVar(){
	if (firstArgs)
		return form2->getFirstArgument();
	else
		return form2->getSecondArgument();
}

ProofLabel* EqLabel::makeNewLabel(){
	return new EqLabel(form);
}

ProofLabel* FuncLabel::makeNewLabel() {
	return new FuncLabel(form1, form2);
}

ProofLabel* SelfLabel::makeNewLabel() {
	return this;
}

ProofLabel* ConstructorLabel::makeNewLabel() {
	return new ConstructorLabel(form1, form2, firstArgs);
}
