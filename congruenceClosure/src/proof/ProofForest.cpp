/*
 * ProofForest.cpp
 *
 *  Created on: 23-jan.-2014
 *      Author: Koen
 */

#include "ProofForest.hpp"
#include <unordered_set>
#include <iostream>

ProofNode* getRoot(ProofNode* a){
	ProofNode* r = a;
	while(!r->isRoot()){
		r = r->getParent();
	}
	return r;
}

ProofForest::ProofForest() {
}

ProofForest::~ProofForest(){
}

void ProofForest::addNode(Variable& a){
	proofNodes.insert({a.getVarId(),shared_ptr<ProofNode>(new ProofNode(a))});
}

void ProofForest::addEdge(ProofLabel& label, int timestamp){
	ProofNode* a = getNode(label.getFirstVar());
	ProofNode* b = getNode(label.getSecondVar());
	invertAllEdgesToRoot(a);
	setEdgeWLabel(a,b,label, timestamp); // b will be a's parent
	a->transferIneqEdgesToRoot();
}

void ProofForest::addIneqEdge(ConstInEq* ineq){
	ProofNode* ra = getRoot(getNode(ineq->getLeftSide()));
	ProofNode* rb = getRoot(getNode(ineq->getRightSide()));
	ra->addIneqEdge(rb,ineq);
}

void ProofForest::setEdgeWLabel(ProofNode* a, ProofNode* b, ProofLabel& label, int timestamp){
	a->setParent(b);
	a->setLabel(label, timestamp);
}

void ProofForest::invertAllEdgesToRoot(ProofNode* a){
	ProofNode* prevNode = a;
	ProofNode* rootNode = getRoot(a);
	ProofNode* currentNode = a;
	ProofNode* nextNode; ProofLabel* nextLabel;
	ProofLabel* label = & SelfLabel::getInstance();
	while (prevNode != rootNode){
		nextNode = currentNode->getParent();
		nextLabel = currentNode->getLabel()->makeNewLabel();
		int ts = currentNode->getEdgeTimestamp();
		setEdgeWLabel(currentNode, prevNode, *label, ts);
		if (!label->isSelfLabel())
			delete label;
		label = nextLabel;
		prevNode = currentNode;
		currentNode = nextNode;
	}
	rootNode->transferIneqEdgesToRoot();
}

unordered_set<Formula*> ProofForest::explain(Variable a, Variable b){
	Variable c = nearestCommonAncestor(a,b);
	unordered_set<Formula*> expl1 = explainPath(a,c);
	unordered_set<Formula*> expl2 = explainPath(b,c);
	expl1.insert(expl2.begin(), expl2.end());
	return expl1;
}

/*
 * b is ancestor of a
 */
unordered_set<Formula*> ProofForest::explainPath(Variable a, Variable b){
	unordered_set<Formula*> explanation;
	ProofNode* nodeA = getNode(a);	ProofNode* nodeB = getNode(b);
	while (nodeA != nodeB) {
		if (nodeA->isRoot())
			throw string("b must be an ancestor of a");
		unordered_set<Formula*> expl = nodeA->getLabel()->explain(this);
		explanation.insert(expl.begin(), expl.end());
		nodeA = nodeA->getParent();
	}
	return explanation;
}

Variable ProofForest::nearestCommonAncestor(Variable a, Variable b) {
	//TODO find optimal algorithm
	unordered_set<ProofNode*> ancA = unordered_set<ProofNode*>();
	unordered_set<ProofNode*> ancB = unordered_set<ProofNode*>();
	ProofNode* nodeA = getNode(a);	ProofNode* nodeB = getNode(b);
	ancA.insert(nodeA); ancB.insert(nodeB);
	while(ancA.count(nodeB)==0 && ancB.count(nodeA)==0) {
		if (nodeA->isRoot() && nodeB->isRoot())
			throw string("no common ancestor found");
		nodeA = nodeA->getParent();
		nodeB = nodeB->getParent();
		ancA.insert(nodeA); ancB.insert(nodeB);
	}
	if (ancA.count(nodeB)>0)
		return nodeB->getVariable();
	else
		return nodeA->getVariable();
}

unordered_set<Formula*> ProofForest::explainConflict(Variable a, Variable b) {
	ProofNode* na = getNode(a); ProofNode* nb = getNode(b);
	ProofNode* ra = getRoot(na); ProofNode* rb = getRoot(nb);

	if (! ra->hasIneqEdge(rb))
		throw string("No conflict between " + a.toString() + " (root " + ra->getVariable().toString()
				+ ") and " + b.toString() + " (root " + rb->getVariable().toString() + ")");

	unordered_set<Formula*> expl1;
	unordered_set<Formula*> expl2;

	ConstInEq* conflictForm = ra->getConflictFormula(rb);
	Variable c1 = conflictForm->getLeftSide();
	Variable c2 = conflictForm->getRightSide();
	if (getRoot(getNode(c1)) == ra) { // c1 = ca
		expl1 = explain(a,c1);
		expl2 = explain(b,c2);
	} else {
		expl1 = explain(a,c2); // c2 = ca
		expl2 = explain(b,c1);
	}
	expl1.insert(expl2.begin(), expl2.end());
	expl1.insert(conflictForm);
	return expl1;
}

ProofNode* ProofForest::getNode(const Variable a){
	return proofNodes.at(a.getVarId()).get();
}

void ProofForest::revertTo(int decisionlevel){
	for (auto it = proofNodes.begin(); it != proofNodes.end(); it++) {
		it->second->revertTo(decisionlevel);
	}
}

ProofNode::ProofNode(Variable& a) : variable(a){
	parent = this;
	edgeLabel = SelfLabel::getInstance().makeNewLabel();
	edgeTimeStamp = 0;
}

ProofNode::~ProofNode() {
	if (!edgeLabel->isSelfLabel())
		delete edgeLabel;
}

bool ProofNode::isRoot(){
	return edgeLabel->isSelfLabel();
}

void ProofNode::setLabel(ProofLabel& label, int timestamp) {
	if (!edgeLabel->isSelfLabel())
		delete edgeLabel;
	edgeLabel = label.makeNewLabel();
	edgeTimeStamp = timestamp;
}

void ProofNode::addIneqEdge(ProofNode* rb, ConstInEq* form) {
	if (!isRoot() || !rb->isRoot())
		throw string("Inequality edges can only be added to root-nodes");
	ineqEdges[rb] = form;
	if (!rb->hasIneqEdge(this))
		rb->addIneqEdge(this, form);
}

void ProofNode::transferIneqEdgesToRoot() {
	if (isRoot())
		return;
	ProofNode* rb = getRoot(this);
	for (std::map<ProofNode*, ConstInEq*>::iterator it = ineqEdges.begin(); it != ineqEdges.end(); it++) {
		rb->addIneqEdge(it->first, it->second);
	}
	clearIneqEdges();
}

void ProofNode::removeIneqEdge(ProofNode* b){
	ineqEdges.erase(b);
//	if (b->hasIneqEdge(this))
//		b->removeIneqEdge(this);
}

void ProofNode::clearIneqEdges() {
	for (std::map<ProofNode*, ConstInEq*>::iterator it = ineqEdges.begin(); it != ineqEdges.end(); it++) {
		it->first->removeIneqEdge(this);
	}
	ineqEdges.clear();
}

void ProofNode::revertTo(int decisionlevel) {
	if (decisionlevel < edgeTimeStamp) {
		if (!edgeLabel->isSelfLabel())
			delete edgeLabel;
		edgeLabel = SelfLabel::getInstance().makeNewLabel();
		edgeTimeStamp = 0;
	}

	for (auto it = ineqEdgeTimestamps.begin(); it != ineqEdgeTimestamps.end(); it++){
		if (decisionlevel < it->second)
			ineqEdges.erase(it->first);
	}
}
