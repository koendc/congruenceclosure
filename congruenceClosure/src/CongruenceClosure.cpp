/*
 * CongruenceClosure.cpp
 *
 *  Created on: 12-nov.-2013
 *      Author: Koen
 */

#include <iostream>
#include "Formulas.hpp"
#include "ConflictException.hpp"

CongruenceClosure::CongruenceClosure() :
	proofForest(new ProofForest()){
	nextVarId = 1;
	currentDecisionLevel = 1;
}

CongruenceClosure::~CongruenceClosure() {
	delete proofForest;
}

void CongruenceClosure::joinClasses(CongruenceClass* Ka, CongruenceClass* Kb, ProofLabel& label) {
	if (Ka->isEqual(Kb)) // If classes are already joined, this function can return
		return;
	if (Ka->hasInequalityEdgeTo(Kb)) { // If there is a formula stating that Kb =/= Ka, there is a conflict
		reportJoinConflict(label);
		return;
	}
	proofForest->addEdge(label, currentDecisionLevel); // add edge to proof forest
	if (Ka->getSize() < Kb->getSize()) {
		CongruenceClass* Ktemp = Ka;
		Ka=Kb;
		Kb=Ktemp;
	}
	for (Node* b : Kb->getNodes()) // combine classes
		Ka->addNode(b, currentDecisionLevel);
	for (CongruenceClass* ineqEdge : Kb->getInequalityEdges()) // combine ineq-edges
		Ka->addInequalityEdgeTo(ineqEdge, currentDecisionLevel);

	// The join may cause more congruences
	list<FormLabel*> pendingJoins = propagateJoins(Ka,Kb);

	for (CongruenceClass* Kc : congClasses) { // redirect the formula-edges to the right class
		if (Kb->hasFormula(Kc) && !Ka->hasFormula(Kc))
			Ka->setFormula(Kc, Kb->getFormula(Kc), currentDecisionLevel);
		if (Kc->hasFormula(Kb))
			Kc->setFormula(Ka, Kc->getFormula(Kb), currentDecisionLevel);
	}

	congClasses.remove(Kb);
	for (FormLabel* flabel : pendingJoins) {
		CongruenceClass* Kc = flabel->getFirstVar().getCongruenceClass();
		CongruenceClass* Kd = flabel->getSecondVar().getCongruenceClass();
		joinClasses(Kc,Kd,*flabel);
	}
	for (auto it=pendingJoins.begin(); it!=pendingJoins.end(); it++){
		delete *it;
	}
}

void CongruenceClosure::reportJoinConflict(ProofLabel& source) {
	throw JoinConflictException(this, source);
}

void CongruenceClosure::reportMergeConflict(ConstInEq* source) {
	throw MergeConflictException(this, source);
}

void CongruenceClosure::merge(Formula& t) {
	t.accept(this);
}

list<FormLabel*> CongruenceClosure::propagateJoins(CongruenceClass* Ka, CongruenceClass* Kb) {
	list<FormLabel*> pendingJoins;
	for (CongruenceClass* Kc : congClasses) {
		if (Ka->hasFormula(Kc) && Kb->hasFormula(Kc)) {
			FuncEq* fa = Ka->getFormula(Kc);
			FuncEq* fb = Kb->getFormula(Kc);
			pendingJoins.push_back(new FuncLabel(fa,fb));
		} else if (Kc->hasFormula(Ka) && Kc->hasFormula(Kb)) {
			FuncEq* fa = Kc->getFormula(Ka);
			FuncEq* fb = Kc->getFormula(Kb);
			pendingJoins.push_back(new FuncLabel(fa,fb));
		}
	}
	if (Ka->hasConstructorFormula() && Kb->hasConstructorFormula()) {
		pendingJoins.push_back(new ConstructorLabel(Ka->getConstructorFormula(), Kb->getConstructorFormula(), true));
		pendingJoins.push_back(new ConstructorLabel(Ka->getConstructorFormula(), Kb->getConstructorFormula(), false));
	}
	return pendingJoins;
}

bool CongruenceClosure::isCongruent(Term& s, Term& t) {
	CongruenceClass* Ks = s.Normalize();
	CongruenceClass* Kt = t.Normalize();
	return Ks->isEqual(Kt);
}

bool CongruenceClosure::isCongruent(Variable& a, Variable& b) {
	ConstTerm s = ConstTerm(&a);
	ConstTerm t = ConstTerm(&b);

	return isCongruent(s, t);
}

string CongruenceClosure::getCongruenceDescr() {
	string result = "Congruence classes:\n";
	for (CongruenceClass* Ka : congClasses) {
		result += Ka->getDescription() + "\n";
	}
	return result;
}

unordered_set<Formula*> CongruenceClosure::explain(Variable a, Variable b) {
	if (!a.getCongruenceClass()->isEqual(b.getCongruenceClass()))
		throw string("Variables are not equal");
	return proofForest->explain(a,b);
}

unordered_set<Formula*> CongruenceClosure::explain(ProofLabel& label) {
	return label.explain(proofForest);
}

unordered_set<Formula*> CongruenceClosure::explainConflict(Variable a, Variable b) {
	return proofForest->explainConflict(a,b);
}

void CongruenceClosure::addIneqEdgeToProofForest(ConstInEq* ineq) {
	proofForest->addIneqEdge(ineq);
}

void CongruenceClosure::addNewVariable(Variable& a) {
	congClasses.insert(congClasses.end(), a.getCongruenceClass());
	proofForest->addNode(a);
}

Variable& CongruenceClosure::makeNewVariable() {
	newVarMutex.lock();
	std::shared_ptr<Variable> newVar(new Variable(nextVarId));
	nextVarId++;
	addNewVariable(*newVar.get());
	variables.push_back(newVar);
	newVarMutex.unlock();
	return *newVar.get();
}

void CongruenceClosure::setDecisionLevel(int newDecisionLevel){
	currentDecisionLevel = newDecisionLevel;
}

void CongruenceClosure::backtrackTo(int decisionlevel) {
	setDecisionLevel(decisionlevel);
	for (CongruenceClass* Ka : congClasses) {
		Ka->revertTo(decisionlevel); // All merges at level $decisionlevel$ and higher need to be reverted.
	}
	proofForest->revertTo(decisionlevel);
}
